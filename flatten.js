let reduce = require("./reduce.js");
let filter = require("./filter.js");


function flatten(elements, depth = 1) {

    if (!Array.isArray(elements)){
        return elements;
    }
    return filter(depth > 0
                ?reduce(elements,(acc, val) => acc.concat(flatten(val, depth - 1)), [])
                :elements.slice(),(element)=>{if(element!==undefined)return true})
}


module.exports = flatten;