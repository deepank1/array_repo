

function reduce(elements, cb, startingValue) {
    
    let index = 0;
    if(!Array.isArray(elements)){
       throw new TypeError("elements reduce is not a function");
    }
    if (elements.length==0 ){
        if(startingValue!==undefined){
            return startingValue;
        }
        else{
           throw new TypeError("Reduce of empty array with no initial value");
        }
    }
    if(startingValue==undefined){
        for(let current=0;current<elements.length;current++){
            if(elements[current]!==undefined){
                startingValue = elements[current];
                index = 1+current;
                break;
            }
        }
        
    }

    for( ; index < elements.length; index++){
        if(elements[index]!==undefined){
            startingValue = cb(startingValue, elements[index],index,elements);
        }
    }
    return startingValue;
}

module.exports = reduce;