let elements = [1,2,3,4,5,5];

function cb(element,index,elements) {
    return element % 2 == 0;
}


let filter = require("./filter.js");

let result = filter(elements, cb);

console.log(result);