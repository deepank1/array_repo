

function map(elements, cb){
    let new_array = []

    for(let index=0;index<elements.length;index++){
        let temp = cb(elements[index],index,elements);
        new_array.push(temp);
    }

    return new_array;
}

module.exports = map;