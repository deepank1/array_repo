

function filter(elements, cb) {
    let new_array = [];

    for(let index=0; index<elements.length; index++){
        if(cb(elements[index],index,elements)===true){
            new_array.push(elements[index]);
        }

    }
    return new_array;
}

module.exports = filter;