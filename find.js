

function find(elements, cb) {
    
    let temp = false;

    for(let index=0; index<elements.length; index++){

        temp = cb(elements[index],index,elements);

        if(temp === true){
            return elements[index];
        }

    }

    if(temp == false){
        return undefined;
    }
}

module.exports = find;